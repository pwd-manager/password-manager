# Password Manager

A password manager is a web application where you can store your passwords in a secure way.
The project contains 2 git submodules as the front-end and back-end like a mono repo, this project contains subversions of Angular UI and Python API.
## Getting started


### Clone Repo recursively
```
$ git clone --recurse-submodules https://gitlab.com/pwd-manager/password-manager.git
```

or

```
$ git clone https://gitlab.com/pwd-manager/password-manager.git
$ git submodule update --init --recursive
```

## Run it

Docker helps us to remove the os dependencies on software development this is why we use it here, please install it to run this project https://docs.docker.com/engine/install/

After having Docker and Docker Compose Installed please notice that we have a docker-compose.yml that defines the running parameters for the respective containers needed, you just need to run one unique command:

```
$ docker-compose up dev
````

This command will start the front-end and back-end for the whole project.

Angular UI on http://localhost:4200
Python (FastAPI) API on http://localhost:8000


# Algorithm & Approach


## Teck Stack

Since the suggested technologies are Angular and Python, I decided to use the latest stable versions of the frameworks (Angular 14.2.1) (FastAPI 0.88.0) and languages (Typescript 4.7.2) (Python 3.11 ), to benefit from the latest features and fixes.

## API

### Python 3.11
Python has increased the support and functionality for types hints (typing library and native features), in each new version launched, so this is a good reason to use this version, also it improves error tracebacks, async programming, exception groups, regular expressions and is up to 10-60% faster than Python 3.10.
https://www.python.org/downloads/release/python-3110/

### FastAPI 0.88.0
This modern, fast high-performance, web framework help me to build the API based on standard Python type hints.
I choose it because of its fast performance, fast code, intuitive short and easy, also robust, and production-ready server, and OpenAPI auto-documentation.
https://fastapi.tiangolo.com/

## UI
### Angular 14.2
This last version of Angular allows me to understand and solve the compile errors, since they improve a lot in that part, also this version came with other new features...
https://blog.angular.io/angular-v14-is-now-available-391a6db736af


### Angular Material 14.2

I choose to use Angular Material (Mat) because it helps me to implement elaborate UI components and controls without developing from scratch, and has a strong connection with the ts component (controller) so is easy to catch events and bind data with the template.
https://material.angular.io/

### Bootstrap 5.2
I choose to include bootstrap since is an easy CSS framework/library, I diced to use it like a library and not like a framework since Angular Material is the priority,  it helps me to add already short style classes and gives me other icon resources. also is pretty easy to implement a responsive design with it.

https://getbootstrap.com/docs/5.0/getting-started/introduction/
